#include "motor.hpp"


Motor motorRight; 
Motor motorLeft; 

void setupMotors(){
  motorRight.init(5, 4);
  motorLeft.init(6, 7);
  //setMotorPWM(50, false, 50, true);
}
void setMotorPWM(uint8_t pwmRight, bool dirRight, uint8_t pwmLeft, bool dirLeft){
  motorRight.setForce(pwmRight, dirRight);
  motorLeft.setForce(pwmLeft, !dirRight);
}
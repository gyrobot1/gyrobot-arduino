#include "serial.hpp"
#include "motor.hpp"

SerialState state;
int dataCounter = 0;
uint8_t pwmRight, pwmLeft;
bool dirLeft, dirRight;

void setupSerial(){
  Serial.begin(115200);
  state = SerialState::End;
}

void readSerialData(uint8_t d){
  switch (dataCounter){
    case 1:
      dirRight = d & 0b01;
      dirLeft = !dirRight;
      break;
    case 2:
      pwmRight = d;
      break;
    case 3:
      pwmLeft = d;
      break;
  }  
}

void parse(uint8_t d){
  switch (state) {
    case SerialState::End:
      if (d == DATASTART){
        state = SerialState::Start;
        dataCounter = 0;
      } 
      break;
    case SerialState::Start:
      dataCounter++;
      if (dataCounter>3){
        setMotorPWM(pwmRight, dirRight, pwmLeft, dirLeft);
        if (d == DATAEND) {
          state = SerialState::End;
          break;
        }
      } else readSerialData(d);
      break;
  }
}

void handleSerialData(){
  uint8_t d = Serial.read();
  parse(d);
}

void sendGyroData(GyroData &data) {
  char *d = (char*)&data;
  Serial.write(DATASTART);
  Serial.write(d, sizeof(GyroData));
  Serial.write(DATAEND);
}


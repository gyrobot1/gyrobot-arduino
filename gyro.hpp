#ifndef GYRO_H
#define GYRO_H

#include "Arduino.h"
#include "Wire.h" 

typedef struct GyroData {
  float acc_x; 
  float acc_y;
  float acc_z; 
  float gyro_x;
  float gyro_y;
  float gyro_z; 
};


void setupGyro();
void getGyroData(GyroData &data);

#endif
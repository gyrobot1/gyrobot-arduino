#include "gyro.hpp"
#include "mpu9250.h"

const int MPU_ADDR = 0x68; 

bfs::Mpu9250 imu;

void setupGyro(){
  Wire.begin();
  Wire.setClock(400000);
  imu.Config(&Wire, bfs::Mpu9250::I2C_ADDR_PRIM);
  if (!imu.Begin()) {
    Serial.println("Error initializing communication with IMU");
    while(1) {}
  }
  if (!imu.ConfigSrd(19)) {
    Serial.println("Error configured SRD");
    while(1) {}
  }
}

void getGyroData(GyroData &data){
  if (!imu.Read()) return;
  if (!imu.new_imu_data()) return;  
  data.acc_x = imu.accel_x_mps2();
  data.acc_y = imu.accel_y_mps2();
  data.acc_z = imu.accel_z_mps2();
  data.gyro_x = imu.gyro_x_radps();
  data.gyro_y = imu.gyro_y_radps();
  data.gyro_z = imu.gyro_z_radps();
  return data;
}
#ifndef SERIAL_H
#define SERIAL_H

#include "gyro.hpp"

#define DATASTART 170
#define DATAEND 85

enum SerialState {
  None = 0,
  Start = 1,
  Data = 2,
  End = 3
}; 

void setupSerial();
void handleSerialData();
void sendGyroData(GyroData &data);

#endif
#ifndef MOTOR_H
#define MOTOR_H

#include "Arduino.h"

typedef struct Motor {
  uint8_t PWMPin;
  uint8_t DirPin;
  void init(uint8_t pp, uint8_t dp){
    this->PWMPin = pp;
    this->DirPin = dp;
    pinMode(this->PWMPin, OUTPUT);
    pinMode(this->DirPin, OUTPUT);
    digitalWrite(this->DirPin, LOW); 
  }
  void setForce(uint8_t pwmLevel, bool dir){
    if (dir){
      pinMode(this->DirPin, OUTPUT); 
      digitalWrite(this->DirPin, LOW); 
    } else {
      pinMode(this->DirPin, INPUT);
    }
    analogWrite(this->PWMPin, pwmLevel);
  }
};

void setupMotors();
void setMotorPWM(uint8_t pwmRight, bool dirRight, uint8_t pwmLeft, bool dirLeft);

#endif
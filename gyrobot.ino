#include "gyro.hpp" 
#include "motor.hpp"
#include "serial.hpp"

GyroData data;

void setup() {
  setupMotors();
  setupSerial();
  setupGyro();
}

void loop() {
  getGyroData(data);
  sendGyroData(data);
}

void serialEvent() {
  while (Serial.available()) {
    handleSerialData();
  }
}